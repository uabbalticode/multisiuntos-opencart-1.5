<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
	  <?php /*
	  <div class="vtabs"><a href="#tab-general"><?php echo $tab_general; ?></a>
        <?php foreach ($geo_zones as $geo_zone) { ?>
        <a href="#tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></a>
        <?php } ?>
      </div>
      */ ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general" <?php /*class="vtabs-content" */ ?>>
          <table class="form">
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="multisiuntos_status">
                  <?php if ($multisiuntos_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
		  
		  
		  <table id="module" class="list">
			<thead>
			  <tr>
				<td class="left"><?php echo $entry_shipping_method; ?></td>
				<td class="left"><?php echo $entry_courier_id; ?></td>
				<td class="left"><?php echo $entry_express; ?></td>
				<td class="left"><?php echo $entry_pickup_required; ?></td>
				<td class="left"><?php echo $entry_loading_required; ?></td>
				<td class="left"><?php echo $entry_saturday; ?></td>
				<td class="left"><?php echo $entry_insurance; ?></td>
				<td class="left"><?php echo $entry_proof; ?></td>
				<td></td>
			  </tr>
			</thead>
			<?php $module_row = 0; ?>
			<?php foreach ($modules as $module) { ?>
			<tbody id="module-row<?php echo $module_row; ?>">
			  <tr>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][shipping_method]">
					<?php foreach ($shipping_methods as $shipping_method) { ?>
					<?php if ($shipping_method['shipping_method'] == $module['shipping_method']) { ?>
					<option value="<?php echo $shipping_method['shipping_method']; ?>" selected="selected"><?php echo $shipping_method['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $shipping_method['shipping_method']; ?>"><?php echo $shipping_method['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][courier_id]">
					<?php foreach ($courier_ids as $courier_id) { ?>
					<?php if ($courier_id['courier_id'] == $module['courier_id']) { ?>
					<option value="<?php echo $courier_id['courier_id']; ?>" selected="selected"><?php echo $courier_id['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $courier_id['courier_id']; ?>"><?php echo $courier_id['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][express]">
					<?php if ($module['express']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][pickup_required]">
					<?php if ($module['pickup_required']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][loading_required]">
					<?php if ($module['loading_required']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][saturday]">
					<?php if ($module['saturday']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][insurance]">
					<?php if ($module['insurance']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="left"><select name="multisiuntos_module[<?php echo $module_row; ?>][proof]">
					<?php if ($module['proof']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
			  </tr>
			</tbody>
			<?php $module_row++; ?>
			<?php } ?>
			<tfoot>
			  <tr>
				<td colspan="8"></td>
				<td class="left"><a onclick="addModule();" class="button"><span><?php echo $button_add_module; ?></span></a></td>
			  </tr>
			</tfoot>
		  </table>
		  
		  
		  
        </div>
        <?php /*foreach ($geo_zones as $geo_zone) { ?>
        <div id="tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="multisiuntos_<?php echo $geo_zone['geo_zone_id']; ?>_status">
                  <?php if (${'multisiuntos_' . $geo_zone['geo_zone_id'] . '_status'}) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
        <?php }*/ ?>
      </form>
	  

	  
	  
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';	
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][shipping_method]">';
	<?php foreach ($shipping_methods as $shipping_method) { ?>
	html += '      <option value="<?php echo $shipping_method['shipping_method']; ?>"><?php echo $shipping_method['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][courier_id]">';
	<?php foreach ($courier_ids as $courier_id) { ?>
	html += '      <option value="<?php echo $courier_id['courier_id']; ?>"><?php echo $courier_id['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][express]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][pickup_required]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][loading_required]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][saturday]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][insurance]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="multisiuntos_module[' + module_row + '][proof]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}

$('.vtabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>