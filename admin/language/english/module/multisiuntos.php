<?php
// Heading
$_['heading_title']    = 'Multi Siuntos';

// Text
$_['text_module']      = 'Modules';
$_['text_ms_success']     = 'Success: You have modified module multisiuntos!';
$_['text_multisiuntos_xml']      = 'Multishipping XML';
$_['text_select_error']      = 'Select order';


// Entry
$_['entry_ms_status']     = 'Status:';
$_['entry_shipping_method']     = 'Shipping method:';
$_['entry_courier_id']     = 'Courier identification:';
$_['entry_express']     = 'Express:';
$_['entry_pickup_required']     = 'pickup is required:';
$_['entry_loading_required']     = 'Loading is required:';
$_['entry_saturday']     = 'Saturday:';
$_['entry_insurance']     = 'Insurance:';
$_['entry_proof']     = 'Proof:';

// Error
$_['error_ms_permission'] = 'Warning: You do not have permission to modify module multisiuntos!';
?>